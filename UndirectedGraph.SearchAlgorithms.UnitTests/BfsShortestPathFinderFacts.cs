﻿using System;
using System.Collections.Generic;
using System.Linq;
using UndirectedGraph.Data.Entities;
using UndirectedGraph.SearchAlgorithms.ShortestPath;
using Xunit;

namespace UndirectedGraph.SearchAlgorithms.UnitTests
{
    public class BfsShortestPathFinderFacts
    {
        public class TheFindShortestPathMethod
        {
            private readonly BfsShortestPathFinder _pathFinder;

            public TheFindShortestPathMethod()
            {
                _pathFinder = new BfsShortestPathFinder();
            }

            [Fact]
            public void ThrowsArgumentNullExceptionForNullFromArgument()
            {
                Assert.Throws<ArgumentNullException>(() => _pathFinder.FindShortestPath(null, new Node(), new Adjacency[] { } ));
            }

            [Fact]
            public void ThrowsArgumentNullExceptionForNullToArgument()
            {
                Assert.Throws<ArgumentNullException>(() => _pathFinder.FindShortestPath(new Node(), null, new Adjacency[] { }));
            }

            [Fact]
            public void ThrowsArgumentNullExceptionForNullAdjacenciesArgument()
            {
                Assert.Throws<ArgumentNullException>(() => _pathFinder.FindShortestPath(new Node(), new Node(), null));
            }

            [Fact]
            public void ReturnsNullForNotExistingPathBetweenNodes()
            {
                var result = _pathFinder.FindShortestPath(new Node(1, "Node1"), new Node(2, "Node2"), new Adjacency[] { });

                Assert.Null(result);
            }

            [Fact]
            public void ReturnsEmptyCollectionForPathToTheStartNode()
            {
                var result = _pathFinder.FindShortestPath(NewNode(1), NewNode(1), new[] { NewAdj(1, 2) });

                Assert.Empty(result);
            }

            [Fact]
            public void ReturnsOneNodeForOneHopLongPathBetweenNodes()
            {
                var result = _pathFinder.FindShortestPath(NewNode(1), NewNode(2), new [] { NewAdj(1,2) });

                Assert.Equal(1, result.Count());
            }

            [Fact]
            public void ReturnsMultipleNodesForMultipleHopsLongPathBetweenNodes()
            {
                var result = _pathFinder.FindShortestPath(NewNode(1), NewNode(4), new[] { NewAdj(1, 2), NewAdj(2, 3), NewAdj(3, 4) });

                Assert.Equal(3, result.Count());
            }

            [Fact]
            public void ReturnsShortestPathIfMultiplePathsAreAvailableBetweenNodes()
            {
                var result = _pathFinder.FindShortestPath(NewNode(1), NewNode(6), new[] { NewAdj(1, 2), NewAdj(2, 3), NewAdj(3, 4), NewAdj(4, 5), NewAdj(5, 6), NewAdj(3, 6), NewAdj(4, 6) });

                Assert.Equal(3, result.Count());
            }

            [Fact]
            public void ReturnsShortestPathForMultipleAvailablePathsWithCorrectOrder()
            {
                var result = _pathFinder.FindShortestPath(NewNode(1), NewNode(6), new[] { NewAdj(1, 2), NewAdj(2, 3), NewAdj(3, 4), NewAdj(4, 5), NewAdj(5, 6), NewAdj(3, 6), NewAdj(4, 6) });

                var nodes = result.ToList();
                Assert.Equal(2, nodes[0].ExternalId);
                Assert.Equal(3, nodes[1].ExternalId);
                Assert.Equal(6, nodes[2].ExternalId);
            }

            [Fact]
            public void ReturnsShortestPathForMultipleAvailablePathsWithCorrectProperties()
            {
                var result = _pathFinder.FindShortestPath(NewNode(1), NewNode(6), new[] { NewAdj(1, 2), NewAdj(2, 3), NewAdj(3, 4), NewAdj(4, 5), NewAdj(5, 6), NewAdj(3, 6), NewAdj(4, 6) });

                var nodes = result.ToList();
                Assert.Equal("Node2", nodes[0].Label);
                Assert.Equal("Node3", nodes[1].Label);
                Assert.Equal("Node6", nodes[2].Label);
            }

            #region Private Helper Methods

            private Adjacency NewAdj(int node1Id, int node2Id)
            {
                return new Adjacency
                {
                    Node1Id = node1Id,
                    Node1 = NewNode(node1Id),
                    Node2Id = node2Id,
                    Node2 = NewNode(node2Id)
                };
            }

            private Node NewNode(int externalId)
            {
                return new Node(externalId, string.Format("Node{0}", externalId));
            }

            #endregion

        }
    }
}
