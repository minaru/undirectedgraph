﻿using System.Collections.Generic;
using System.ServiceModel;
using UndirectedGraph.Contracts.Data;
using UndirectedGraph.Contracts.Service;

namespace UndirectedGraph.Proxies
{
    public class GraphPathClient : ClientBase<IGraphPathService>, IGraphPathService
    {
        public IEnumerable<NodeData> FindShortestPath(NodeData from, NodeData to)
        {
            return Channel.FindShortestPath(from, to);
        }
    }
}
