﻿namespace UndirectedGraph.Data.Entities
{
    public class Adjacency
    {
        public Adjacency()
        {

        }

        public Adjacency(int node1Id, int node2Id)
        {
            Node1Id = node1Id;
            Node2Id = node2Id;
        }

        public int Id { get; set; }

        public int Node1Id { get; set; }
        public Node Node1 { get; set; }

        public int Node2Id { get; set; }
        public Node Node2 { get; set; }
    }
}
