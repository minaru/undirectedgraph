﻿using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using UndirectedGraph.Contracts.Data;
using UndirectedGraph.Data.Entities;
using UndirectedGraph.Data.Repositories;
using UndirectedGraph.SearchAlgorithms.ShortestPath;
using Xunit;

namespace UndirectedGraph.WebService.UnitTests
{
    public class GraphManagerFacts
    {
        public class TheConstructor
        {
            [Fact]
            public void ThrowsArgumentNullExceptionForNullParameterAsGraphRepository()
            {
                Assert.Throws<ArgumentNullException>(() => new GraphManager(null, new Mock<IShortestPathFinder>().Object));
            }

            [Fact]
            public void ThrowsArgumentNullExceptionForNullParameterAsPathFinder()
            {
                Assert.Throws<ArgumentNullException>(() => new GraphManager(new Mock<IGraphRepository>().Object, null));
            }
        }

        public class TheSaveGraphMethod
        {

            [Fact]
            public void ThrowsArgumentNullExceptionForNullGraphDataArgument()
            {
                var repository = new Mock<IGraphRepository>();
                var graphManager = CreateGraphManager(repository);

                Assert.Throws<ArgumentNullException>(() => graphManager.SaveGraph(null) );
            }

            [Fact]
            public void SavesOneNodeForOneNodeGraph()
            {
                var repository = new Mock<IGraphRepository>();
                var nodeData = new[] { new NodeData { Id = 1, Label = "Node1" } };
                var adjacencyData = new NodeAdjacencyData[0];
                var graphData = new GraphData { Nodes = nodeData, NodeAdjacencies = adjacencyData };

                var graphManager = CreateGraphManager(repository);
                graphManager.SaveGraph(graphData);

                repository.Verify(r => r.ReplaceAndSave(It.IsAny<IEnumerable<Node>>(), It.IsAny<IEnumerable<Adjacency>>()), Times.Once);
                repository.Verify(r => r.ReplaceAndSave(It.Is<IEnumerable<Node>>(nodes => nodes.Count() == 1), It.Is<IEnumerable<Adjacency>>(adjacencies => adjacencies.Count() == 0)), Times.Once);
            }

            [Fact]
            public void SavesOneNodeWithCorrectPropertiesForOneNodeGraph()
            {
                var repository = new Mock<IGraphRepository>();
                var nodeData = new[] { new NodeData { Id = 1, Label = "Node1" } };
                var adjacencyData = new NodeAdjacencyData[0];
                var graphData = new GraphData { Nodes = nodeData, NodeAdjacencies = adjacencyData };

                var graphManager = CreateGraphManager(repository);
                graphManager.SaveGraph(graphData);

                repository.Verify(r => r.ReplaceAndSave(It.Is<IEnumerable<Node>>(nodes => nodes.Single().ExternalId == nodeData.Single().Id && nodes.Single().Label == nodeData.Single().Label), It.IsAny<IEnumerable<Adjacency>>()), Times.Once);
            }

            [Fact]
            public void SavesMultipleNodesForMultipleNodesGraphWithoutAdjacencies()
            {
                var repository = new Mock<IGraphRepository>();
                var nodeData = new[] { new NodeData { Id = 1, Label = "Node1" }, new NodeData { Id = 2, Label = "Node2" }, new NodeData { Id = 3, Label = "Node3" } };
                var adjacencyData = new NodeAdjacencyData[0];
                var graphData = new GraphData { Nodes = nodeData, NodeAdjacencies = adjacencyData };

                var graphManager = CreateGraphManager(repository);
                graphManager.SaveGraph(graphData);

                repository.Verify(r => r.ReplaceAndSave(It.Is<IEnumerable<Node>>(nodes => nodes.Count() == nodeData.Length), It.Is<IEnumerable<Adjacency>>(adjacencies => adjacencies.Count() == 0)), Times.Once);
            }

            [Fact]
            public void SavesMultipleNodesAndOneAdjacencyForMultipleNodesGraphWithOneAdjacency()
            {
                var repository = new Mock<IGraphRepository>();
                var nodeData = new[] { new NodeData { Id = 1, Label = "Node1" }, new NodeData { Id = 2, Label = "Node2" }, new NodeData { Id = 3, Label = "Node3" } };
                var adjacencyData = new[] { new NodeAdjacencyData { Node1Id = 1, Node2Id = 2 } };
                var graphData = new GraphData { Nodes = nodeData, NodeAdjacencies = adjacencyData };

                var graphManager = CreateGraphManager(repository);
                graphManager.SaveGraph(graphData);

                repository.Verify(r => r.ReplaceAndSave(It.Is<IEnumerable<Node>>(nodes => nodes.Count() == nodeData.Length), It.Is<IEnumerable<Adjacency>>(adjacencies => adjacencies.Count() == adjacencyData.Length)), Times.Once);
            }

            [Fact]
            public void SavesMultipleNodesAndOneAdjacencyWithCorrectPropertiesForMultipleNodesGraphWithOneAdjacency()
            {
                var repository = new Mock<IGraphRepository>();
                var nodeData = new[] { new NodeData { Id = 1, Label = "Node1" }, new NodeData { Id = 2, Label = "Node2" }, new NodeData { Id = 3, Label = "Node3" } };
                var adjacencyData = new[] { new NodeAdjacencyData { Node1Id = 1, Node2Id = 2 } };
                var graphData = new GraphData { Nodes = nodeData, NodeAdjacencies = adjacencyData };

                var graphManager = CreateGraphManager(repository);
                graphManager.SaveGraph(graphData);

                repository.Verify(r => r.ReplaceAndSave(It.IsAny<IEnumerable<Node>>(), It.Is<IEnumerable<Adjacency>>(adjacencies => IsMatchingAdjacency(adjacencies.Single(), 1, 2))), Times.Once);
            }

            [Fact]
            public void SavesMultipleNodesAndMultipleAdjacenciesForMultipleNodesGraphWithMultipleAdjacencies()
            {
                var repository = new Mock<IGraphRepository>();
                var nodeData = new[] { new NodeData { Id = 1, Label = "Node1" }, new NodeData { Id = 2, Label = "Node2" }, new NodeData { Id = 3, Label = "Node3" } };
                var adjacencyData = new[] { new NodeAdjacencyData { Node1Id = 1, Node2Id = 2 }, new NodeAdjacencyData { Node1Id = 1, Node2Id = 3 } };
                var graphData = new GraphData { Nodes = nodeData, NodeAdjacencies = adjacencyData };

                var graphManager = CreateGraphManager(repository);
                graphManager.SaveGraph(graphData);

                repository.Verify(r => r.ReplaceAndSave(It.IsAny<IEnumerable<Node>>(), It.Is<IEnumerable<Adjacency>>(adjacencies => adjacencies.Count() == adjacencyData.Length)), Times.Once);
            }

            #region Private Helper Methods

            private GraphManager CreateGraphManager(Mock<IGraphRepository> repository)
            {
                return new GraphManager(repository.Object, new Mock<IShortestPathFinder>().Object);
            }

            private bool IsMatchingAdjacency(Adjacency adjacency, int node1Id, int node2Id)
            {
                return (adjacency.Node1.ExternalId == node1Id && adjacency.Node2.ExternalId == node2Id)
                       || (adjacency.Node1.ExternalId == node2Id && adjacency.Node2.ExternalId == node1Id);
            }

            #endregion
        }

        public class TheFindShortestPathMethod
        {
            [Fact]
            public void ThrowsArgumentNullExceptionForNullFromArgument()
            {
                var repository = new Mock<IGraphRepository>();
                var pathFinder = new Mock<IShortestPathFinder>();
                var graphManager = new GraphManager(repository.Object, pathFinder.Object);
                var toNode = new Mock<NodeData>();

                Assert.Throws<ArgumentNullException>(() => graphManager.FindShortestPath(null, toNode.Object));
            }

            [Fact]
            public void ThrowsArgumentNullExceptionForNullToArgument()
            {
                var repository = new Mock<IGraphRepository>();
                var pathFinder = new Mock<IShortestPathFinder>();
                var graphManager = new GraphManager(repository.Object, pathFinder.Object);
                var fromNode = new Mock<NodeData>();

                Assert.Throws<ArgumentNullException>(() => graphManager.FindShortestPath(fromNode.Object, null));
            }

            [Fact]
            public void NeverCallsLoadAllNodesOnRepository()
            {
                var adjacencies = new[] { new Adjacency(1, 2) };
                var repository = CreateRepositoryMock(adjacencies);
                var pathFinder = CreatePathFinderMock(adjacencies, null);
                var graphManager = new GraphManager(repository.Object, pathFinder.Object);

                graphManager.FindShortestPath(new NodeData { Id = 1 }, new NodeData { Id = 2 });

                repository.Verify(r => r.LoadAllNodes(), Times.Never);
            }

            [Fact]
            public void CallsLoadAllAdjacenciesOnRepositoryOnce()
            {
                var adjacencies = new[] { new Adjacency(1, 2) };
                var repository = CreateRepositoryMock(adjacencies);
                var pathFinder = CreatePathFinderMock(adjacencies, null);
                var graphManager = new GraphManager(repository.Object, pathFinder.Object);

                graphManager.FindShortestPath(new NodeData { Id = 1 }, new NodeData { Id = 2 });

                repository.Verify(r => r.LoadAllAdjacenciesWithNodes(), Times.Once);
            }

            [Fact]
            public void CallsFindShortestPathOnShortestPathFinderOnce()
            {
                var adjacencies = new[] { new Adjacency(1, 2) };
                var repository = CreateRepositoryMock(adjacencies);
                var pathFinder = CreatePathFinderMock(adjacencies, null);
                var graphManager = new GraphManager(repository.Object, pathFinder.Object);

                graphManager.FindShortestPath(new NodeData { Id = 1 }, new NodeData { Id = 2 });

                pathFinder.Verify(pf => pf.FindShortestPath(It.IsAny<Node>(), It.IsAny<Node>(), adjacencies), Times.Once);
            }

            [Fact]
            public void CallsFindShortestPathOnShortestPathFinderWithCorrectExternalIds()
            {
                var adjacencies = new[] { new Adjacency(1, 2) };
                var repository = CreateRepositoryMock(adjacencies);
                var pathFinder = CreatePathFinderMock(adjacencies, null);
                var graphManager = new GraphManager(repository.Object, pathFinder.Object);

                graphManager.FindShortestPath(new NodeData { Id = 1 }, new NodeData { Id = 2 });

                pathFinder.Verify(pf => pf.FindShortestPath(It.Is<Node>(n => n.ExternalId == 1), It.Is<Node>(n => n.ExternalId == 2), adjacencies));
            }

            [Fact]
            public void ReturnsNullForNullResultOfShortestPathFinder()
            {
                var adjacencies = new[] { new Adjacency(1, 2) };
                var repository = CreateRepositoryMock(adjacencies);
                var pathFinder = CreatePathFinderMock(adjacencies, null);
                var graphManager = new GraphManager(repository.Object, pathFinder.Object);

                var result = graphManager.FindShortestPath(new NodeData { Id = 1 }, new NodeData { Id = 3 });

                Assert.Null(result);
            }

            [Fact]
            public void ReturnsEmptyCollectionForEmptyCollectionResultOfShortestPathFinder()
            {
                var adjacencies = new[] { new Adjacency(1, 2) };
                var repository = CreateRepositoryMock(adjacencies);
                var pathFinder = CreatePathFinderMock(adjacencies, new Node[] { });
                var graphManager = new GraphManager(repository.Object, pathFinder.Object);

                var result = graphManager.FindShortestPath(new NodeData { Id = 1 }, new NodeData { Id = 1 });

                Assert.Equal(0, result.Count());
            }

            [Fact]
            public void ReturnsOneNodeForOneNodeResultOfShortestPathFinder()
            {
                var adjacencies = new[] { new Adjacency(1, 2) };
                var repository = CreateRepositoryMock(adjacencies);
                var pathFinder = CreatePathFinderMock(adjacencies, new [] { new Node(2, "Node2") });
                var graphManager = new GraphManager(repository.Object, pathFinder.Object);

                var result = graphManager.FindShortestPath(new NodeData { Id = 1 }, new NodeData { Id = 2 });

                Assert.Equal(1, result.Count());
            }

            [Fact]
            public void ReturnsOneNodeForOneNodeResultOfShortestPathFinderWithCorrectProperties()
            {
                var adjacencies = new[] { new Adjacency(1, 2) };
                var repository = CreateRepositoryMock(adjacencies);
                var pathFinder = CreatePathFinderMock(adjacencies, new[] { new Node(2, "Node2") });
                var graphManager = new GraphManager(repository.Object, pathFinder.Object);

                var result = graphManager.FindShortestPath(new NodeData { Id = 1 }, new NodeData { Id = 2 });

                Assert.Single(result, n => n.Id == 2);
                Assert.Single(result, n => n.Label == "Node2");
            }

            [Fact]
            public void ReturnsMultipleNodesForMultipleNodesResultOfShortestPathFinder()
            {
                var adjacencies = new[] { new Adjacency(1, 2), new Adjacency(2, 3), new Adjacency(3, 4) };
                var repository = CreateRepositoryMock(adjacencies);
                var pathFinder = CreatePathFinderMock(adjacencies, new[] { new Node(2, "Node2"), new Node(3, "Node3"), new Node(4, "Node4") });
                var graphManager = new GraphManager(repository.Object, pathFinder.Object);

                var result = graphManager.FindShortestPath(new NodeData { Id = 1 }, new NodeData { Id = 4 });

                Assert.Equal(3, result.Count());
            }

            #region Private Helper Methods

            private Mock<IGraphRepository> CreateRepositoryMock(IEnumerable<Adjacency> allAdjacencies)
            {
                var repository = new Mock<IGraphRepository>();
                repository.Setup(r => r.LoadAllAdjacenciesWithNodes()).Returns(allAdjacencies);

                return repository;
            }

            private Mock<IShortestPathFinder> CreatePathFinderMock(IEnumerable<Adjacency> adjacencies, IEnumerable<Node> pathNodes)
            {
                var pathFinder = new Mock<IShortestPathFinder>();
                pathFinder.Setup(pf => pf.FindShortestPath(It.IsAny<Node>(), It.IsAny<Node>(), adjacencies)).Returns(pathNodes);

                return pathFinder;
            }

            #endregion
        }

        public class TheLoadGraphMethod
        {
            [Fact]
            public void CallsLoadAllNodesOnRepositoryOnce()
            {
                var nodes = new[] { new Node(1, "Node1"), new Node(2, "Node2") };
                var adjacencies = new[] { new Adjacency(1, 2) };
                var repository = CreateRepositoryMock(nodes, adjacencies);
                var pathFinder = new Mock<IShortestPathFinder>();
                var graphManager = new GraphManager(repository.Object, pathFinder.Object);

                graphManager.LoadGraph();

                repository.Verify(r => r.LoadAllNodes(), Times.Once);
            }

            [Fact]
            public void CallsLoadAllAdjacenciesWithNodesOnRepositoryOnce()
            {
                var nodes = new[] { new Node(1, "Node1"), new Node(2, "Node2") };
                var adjacencies = new[] { new Adjacency(1, 2) };
                var repository = CreateRepositoryMock(nodes, adjacencies);
                var pathFinder = new Mock<IShortestPathFinder>();
                var graphManager = new GraphManager(repository.Object, pathFinder.Object);

                graphManager.LoadGraph();

                repository.Verify(r => r.LoadAllAdjacenciesWithNodes(), Times.Once);
            }

            [Fact]
            public void ReturnsMultipleNodesForMultipleNodesInRepository()
            {
                var nodes = new[] { new Node(1, "Node1"), new Node(2, "Node2") };
                var adjacencies = new[] { new Adjacency(1, 2) };
                var repository = CreateRepositoryMock(nodes, adjacencies);
                var pathFinder = new Mock<IShortestPathFinder>();
                var graphManager = new GraphManager(repository.Object, pathFinder.Object);

                var result = graphManager.LoadGraph();

                var returnedNodes = result.Nodes;
                Assert.Equal(nodes.Length, returnedNodes.Count());
            }

            [Fact]
            public void ReturnsMultipleNodesWithCorrectPropertiesForMultipleNodesInRepository()
            {
                var nodes = new[] { new Node(1, "Node1"), new Node(2, "Node2") };
                var adjacencies = new[] { new Adjacency(1, 2) };
                var repository = CreateRepositoryMock(nodes, adjacencies);
                var pathFinder = new Mock<IShortestPathFinder>();
                var graphManager = new GraphManager(repository.Object, pathFinder.Object);

                var result = graphManager.LoadGraph();

                var returnedNodes = result.Nodes;
                Assert.Single(returnedNodes, n => n.Id == 1 && n.Label == "Node1");
                Assert.Single(returnedNodes, n => n.Id == 2 && n.Label == "Node2");
            }

            [Fact]
            public void ReturnsMultipleAdjacenciesForMultipleAdjacenciesInRepository()
            {
                var nodes = new[] { new Node(1, "Node1"), new Node(2, "Node2"), new Node(3, "Node3") };
                var adjacencies = new[] { new Adjacency { Node1 = NewNode(1), Node2 = NewNode(2) }, new Adjacency { Node1 = NewNode(2), Node2 = NewNode(3) }, new Adjacency { Node1 = NewNode(1), Node2 = NewNode(3) } };
                var repository = CreateRepositoryMock(nodes, adjacencies);
                var pathFinder = new Mock<IShortestPathFinder>();
                var graphManager = new GraphManager(repository.Object, pathFinder.Object);

                var result = graphManager.LoadGraph();

                var returnedAdjacencies = result.NodeAdjacencies;
                Assert.Equal(adjacencies.Length, returnedAdjacencies.Count());
            }

            [Fact]
            public void ReturnsMultipleAdjacenciesWithCorrectPropertiesForMultipleAdjacenciesInRepository()
            {
                var nodes = new[] { new Node(1, "Node1"), new Node(2, "Node2"), new Node(3, "Node3") };
                var adjacencies = new[] { new Adjacency { Node1 = NewNode(1), Node2 = NewNode(2) }, new Adjacency { Node1 = NewNode(2), Node2 = NewNode(3) }, new Adjacency { Node1 = NewNode(1), Node2 = NewNode(3) } };
                var repository = CreateRepositoryMock(nodes, adjacencies);
                var pathFinder = new Mock<IShortestPathFinder>();
                var graphManager = new GraphManager(repository.Object, pathFinder.Object);

                var result = graphManager.LoadGraph();

                var returnedAdjacencies = result.NodeAdjacencies;
                Assert.Single(returnedAdjacencies, a => (a.Node1Id == 1 && a.Node2Id == 2) || (a.Node1Id == 2 && a.Node2Id == 1) );
                Assert.Single(returnedAdjacencies, a => (a.Node1Id == 3 && a.Node2Id == 2) || (a.Node1Id == 2 && a.Node2Id == 3));
                Assert.Single(returnedAdjacencies, a => (a.Node1Id == 1 && a.Node2Id == 3) || (a.Node1Id == 3 && a.Node2Id == 1));
            }

            [Fact]
            public void ReturnsEmptyNodesForNoNodeInRepository()
            {
                var nodes = new Node[] { };
                var adjacencies = new Adjacency[] { };
                var repository = CreateRepositoryMock(nodes, adjacencies);
                var pathFinder = new Mock<IShortestPathFinder>();
                var graphManager = new GraphManager(repository.Object, pathFinder.Object);

                var result = graphManager.LoadGraph();

                var returnedNodes = result.Nodes;
                Assert.Empty(returnedNodes);
            }

            [Fact]
            public void ReturnsEmptyAdjacenciesForNoAdjacencyInRepository()
            {
                var nodes = new Node[] { };
                var adjacencies = new Adjacency[] { };
                var repository = CreateRepositoryMock(nodes, adjacencies);
                var pathFinder = new Mock<IShortestPathFinder>();
                var graphManager = new GraphManager(repository.Object, pathFinder.Object);

                var result = graphManager.LoadGraph();

                var returnedAdjacencies = result.NodeAdjacencies;
                Assert.Empty(returnedAdjacencies);
            }

            #region Private Helper Methods

            private Mock<IGraphRepository> CreateRepositoryMock(IEnumerable<Node> allNodes, IEnumerable<Adjacency> allAdjacencies)
            {
                var repository = new Mock<IGraphRepository>();
                repository.Setup(r => r.LoadAllAdjacenciesWithNodes()).Returns(allAdjacencies);
                repository.Setup(r => r.LoadAllNodes()).Returns(allNodes);

                return repository;
            }

            private Node NewNode(int id)
            {
                return new Node(id, string.Format("Node{0}", id));
            }

            #endregion

        }
    }
}
