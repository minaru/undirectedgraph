﻿using System.Collections.Generic;
using System.Web.Mvc;
using UndirectedGraph.Contracts.Data;
using UndirectedGraph.Proxies;

namespace UndirectedGraph.WebClient.Controllers
{
    public class GraphController : Controller
    {
        public JsonResult Load()
        {
            GraphData graph;
            bool isError;
            try
            {
                var client = new GraphLoadClient();
                graph = client.LoadGraph();
                isError = false;
            }
            catch
            {
                graph = null;
                isError = true;
            }

            var graphResult = new
                                {
                                    Graph = graph,
                                    IsError = isError
                                };
            return Json(graphResult, JsonRequestBehavior.AllowGet);
        }

        public JsonResult CalculateShortestPath(NodeData fromNode, NodeData toNode)
        {
            IEnumerable<NodeData> path = null;
            bool isError;

            try
            {
                var client = new GraphPathClient();
                path = client.FindShortestPath(fromNode, toNode);
                isError = false;
            }
            catch
            {
                isError = true;
            }

            var pathResult = new
                                {
                                    Path = path,
                                    IsError = isError
                                };

            return Json(pathResult, JsonRequestBehavior.DenyGet);
        }
    }
}