﻿using Moq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UndirectedGraph.Contracts.Data;
using UndirectedGraph.DataLoader.GraphException;
using UndirectedGraph.DataLoader.IO;
using UndirectedGraph.DataLoader.Parser;
using Xunit;

namespace UndirectedGraph.DataLoader.UnitTests
{
    public class GraphLoaderFacts
    {
        public class TheConstructor
        {
            [Fact]
            public void ThrowsArgumentNullExceptionForNullParameterAsNodeParser()
            {
                Assert.Throws<ArgumentNullException>( () => new GraphLoader(null) );
            }
        }

        public class TheLoadGraphMethod
        {
            private readonly Mock<INodeParser> _nodeParserMock;

            public TheLoadGraphMethod()
            {
                _nodeParserMock = new Mock<INodeParser>();
            }

            [Fact]
            public void CallsCreateStreamOnceOnEachStreamCreator()
            {
                var parsedNodes = new[] { new ParsedNode(1, "Node1"), new ParsedNode(2, "Node2"), new ParsedNode(3, "Node3") };
                var nodeParserMock = GenerateNodeParser(parsedNodes);
                var streamCreatorMocks = GenerateStreamCreatorMocks(parsedNodes.Length);

                var graphLoader = new GraphLoader(nodeParserMock.Object);
                var result = graphLoader.LoadGraph(streamCreatorMocks.Select(scm => scm.Object));

                foreach (var scm in streamCreatorMocks)
                {
                    scm.Verify(sc => sc.CreateStream(), Times.Once);
                }
            }

            [Fact]
            public void CallsParseNodeOnceOnEachStream()
            {
                var parsedNodes = new[] { new ParsedNode(1, "Node1"), new ParsedNode(2, "Node2"), new ParsedNode(3, "Node3") };
                var nodeParserMock = GenerateNodeParser(parsedNodes);
                var streamCreatorMocks = GenerateStreamCreatorMocks(parsedNodes.Length);

                var graphLoader = new GraphLoader(nodeParserMock.Object);
                var result = graphLoader.LoadGraph(streamCreatorMocks.Select(scm => scm.Object));

                nodeParserMock.Verify(np => np.ParseNode(It.IsAny<Stream>()), Times.Exactly(3));
            }

            [Fact]
            public void ReturnsOneContractNodeWithoutAdjacenciesForSingleNodeWithoutAdjacencies()
            {
                var nodeParserMock = GenerateNodeParser(new[] { new ParsedNode(1, "Node1") } );
                var streamCreatorMock = GenerateStreamCreatorMocks().First();

                var graphLoader = new GraphLoader(nodeParserMock.Object);
                var result = graphLoader.LoadGraph( new[] { streamCreatorMock.Object } );

                streamCreatorMock.Verify(sc => sc.CreateStream(), Times.Once);
                Assert.Empty(result.NodeAdjacencies);
                Assert.Equal(1, result.Nodes.Count());
            }

            [Fact]
            public void ReturnsMultipleContractNodesWithoutAdjacenciesForMultipleNodesWithoutAdjacencies()
            {
                var parsedNodes = new[] { new ParsedNode(1, "Node1"), new ParsedNode(2, "Node2"), new ParsedNode(3, "Node3") };
                var nodeParserMock = GenerateNodeParser(parsedNodes);
                var streamCreatorMocks = GenerateStreamCreatorMocks(parsedNodes.Length);

                var graphLoader = new GraphLoader(nodeParserMock.Object);
                var result = graphLoader.LoadGraph(streamCreatorMocks.Select(scm => scm.Object));

                Assert.Empty(result.NodeAdjacencies);
                Assert.Equal(3, result.Nodes.Count());
            }

            [Fact]
            public void ReturnsMultipleContractNodesWithCorrectPropertiesForMultipleNodes()
            {
                var parsedNodes = new[] { new ParsedNode(1, "Node1"), new ParsedNode(2, "Node2"), new ParsedNode(3, "Node3") };
                var nodeParserMock = GenerateNodeParser(parsedNodes);
                var streamCreatorMocks = GenerateStreamCreatorMocks(parsedNodes.Length);

                var graphLoader = new GraphLoader(nodeParserMock.Object);
                var result = graphLoader.LoadGraph(streamCreatorMocks.Select(scm => scm.Object));

                foreach (var node in parsedNodes)
                {
                    Assert.Single(result.Nodes, n => n.Id == node.Id && n.Label == node.Label);
                }
            }

            [Fact]
            public void ReturnsMultipleContractNodesWithOneAdjacencyForMultipleNodesWithOneAdjacencyDefinedOnce()
            {
                var parsedNodes = new[] { new ParsedNode(1, "Node1", 2), new ParsedNode(2, "Node2"), new ParsedNode(3, "Node3") };
                var nodeParserMock = GenerateNodeParser(parsedNodes);
                var streamCreatorMocks = GenerateStreamCreatorMocks(parsedNodes.Length);

                var graphLoader = new GraphLoader(nodeParserMock.Object);
                var result = graphLoader.LoadGraph(streamCreatorMocks.Select(scm => scm.Object));

                Assert.Equal(1, result.NodeAdjacencies.Count());
            }

            [Fact]
            public void ReturnsMultipleContractNodesWithOneAdjacencyForMultipleNodesWithOneAdjacencyDefinedTwice()
            {
                var parsedNodes = new[] { new ParsedNode(1, "Node1", 2), new ParsedNode(2, "Node2", 1), new ParsedNode(3, "Node3") };
                var nodeParserMock = GenerateNodeParser(parsedNodes);
                var streamCreatorMocks = GenerateStreamCreatorMocks(parsedNodes.Length);

                var graphLoader = new GraphLoader(nodeParserMock.Object);
                var result = graphLoader.LoadGraph(streamCreatorMocks.Select(scm => scm.Object));

                Assert.Equal(1, result.NodeAdjacencies.Count());
            }

            [Fact]
            public void ReturnsMultipleContractNodesWithMultipleAdjacenciesForMultipleNodesWithMultipleAdjacencies()
            {
                var parsedNodes = new[] { new ParsedNode(1, "Node1", 2), new ParsedNode(2, "Node2", 1), new ParsedNode(3, "Node3", 1), new ParsedNode(4, "Node4", 5), new ParsedNode(5, "Node5") };
                var nodeParserMock = GenerateNodeParser(parsedNodes);
                var streamCreatorMocks = GenerateStreamCreatorMocks(parsedNodes.Length);

                var graphLoader = new GraphLoader(nodeParserMock.Object);
                var result = graphLoader.LoadGraph(streamCreatorMocks.Select(scm => scm.Object));

                Assert.Equal(3, result.NodeAdjacencies.Count());
            }

            [Fact]
            public void ReturnsMultipleContractNodesWithMultipleCorrectAdjacenciesForMultipleNodesWithMultipleAdjacencies()
            {
                var parsedNodes = new[] { new ParsedNode(1, "Node1", 2), new ParsedNode(2, "Node2", 1), new ParsedNode(3, "Node3", 1), new ParsedNode(4, "Node4", 5), new ParsedNode(5, "Node5") };
                var nodeParserMock = GenerateNodeParser(parsedNodes);
                var streamCreatorMocks = GenerateStreamCreatorMocks(parsedNodes.Length);

                var graphLoader = new GraphLoader(nodeParserMock.Object);
                var result = graphLoader.LoadGraph(streamCreatorMocks.Select(scm => scm.Object));


                Assert.Single(result.NodeAdjacencies, na => IsAdjacencyMatching(na, parsedNodes[0], parsedNodes[1]));
                Assert.Single(result.NodeAdjacencies, na => IsAdjacencyMatching(na, parsedNodes[0], parsedNodes[2]));
                Assert.Single(result.NodeAdjacencies, na => IsAdjacencyMatching(na, parsedNodes[3], parsedNodes[4]));
            }

            [Fact]
            public void ThrowsGraphParsingExceptionForFormatExceptionThrownInINodeParser()
            {
                var nodeParserMock = GenerateNodeParser(new ParsedNode[0]);
                nodeParserMock.Setup(np => np.ParseNode(It.IsAny<Stream>())).Throws(new FormatException());
                var streamCreatorMock = new Mock<IStreamCreator>();
                streamCreatorMock.SetupGet(sc => sc.StreamSourceDescription).Returns("Path-To-a-File");

                var graphLoader = new GraphLoader(nodeParserMock.Object);
                Assert.Throws<GraphParsingException>(() => graphLoader.LoadGraph(new[] { streamCreatorMock.Object } ));

                streamCreatorMock.Verify(sc => sc.StreamSourceDescription, Times.Once);
            }

            [Fact]
            public void ThrowsGraphAdjacencyExceptionForParsedNodeWithoutNotExistingAdjacencyId()
            {
                var parsedNodes = new[] { new ParsedNode(1, "Node1", 2), new ParsedNode(2, "Node2", 1), new ParsedNode(3, "Node3", 1), new ParsedNode(4, "Node4", 5), new ParsedNode(5, "Node5", 6) };
                var nodeParserMock = GenerateNodeParser(parsedNodes);
                var streamCreatorMocks = GenerateStreamCreatorMocks(parsedNodes.Length);

                var graphLoader = new GraphLoader(nodeParserMock.Object);
                var exception = Assert.Throws<GraphAdjacencyException>(() => graphLoader.LoadGraph(streamCreatorMocks.Select(scm => scm.Object)));

                Assert.Equal(6, exception.AdjacencyId);
                Assert.Equal(5, exception.Node.Id);
            }

            [Fact]
            public void ThrowsSameNodeIdExceptionForParsedNodesWithSameId()
            {
                var parsedNodes = new[] { new ParsedNode(1, "Node1"), new ParsedNode(1, "Node2") };
                var nodeParserMock = GenerateNodeParser(parsedNodes);
                var streamCreatorMocks = GenerateStreamCreatorMocks(parsedNodes.Length);

                var graphLoader = new GraphLoader(nodeParserMock.Object);
                var exception = Assert.Throws<SameNodeIdException>(() => graphLoader.LoadGraph(streamCreatorMocks.Select(scm => scm.Object)));

                Assert.Equal(1, exception.Id);
                Assert.NotNull(exception.Label1);
                Assert.NotNull(exception.Label2);
            }

            #region Private Helper Functions

            private Mock<INodeParser> GenerateNodeParser(IEnumerable<ParsedNode> returnedNodes)
            {
                var nodeParserMock = new Mock<INodeParser>();
                var sequence = nodeParserMock.SetupSequence(np => np.ParseNode(It.IsAny<Stream>()));
                foreach (var node in returnedNodes)
                {
                    sequence.Returns(node);
                }

                return nodeParserMock;
            }

            private IEnumerable<Mock<IStreamCreator>> GenerateStreamCreatorMocks(int count = 1)
            {
                var creators = new Mock<IStreamCreator>[count];
                for (int i = 0; i < creators.Length; i++)
                {
                    var streamCreatorMock = new Mock<IStreamCreator>();
                    streamCreatorMock.Setup(sc => sc.CreateStream()).Returns("".ToStream()).Verifiable();
                    creators[i] = streamCreatorMock;
                }
                return creators;
            }

            private bool IsAdjacencyMatching(NodeAdjacencyData adjacency, ParsedNode node1, ParsedNode node2)
            {
                Func<NodeData, ParsedNode, bool> areNodesMatching = (node, parsedNode) => node.Id == parsedNode.Id && node.Label == parsedNode.Label;

                var adjacentNodeIds = new[] { adjacency.Node1Id, adjacency.Node2Id };

                return adjacentNodeIds.Any(id => id == node1.Id) && adjacentNodeIds.Any(id => id == node2.Id);
            }

            #endregion

        }


    }
}
