﻿using System.Collections.Generic;
using UndirectedGraph.Data.Entities;

namespace UndirectedGraph.SearchAlgorithms.ShortestPath
{
    public interface IShortestPathFinder
    {
        /// <summary>
        /// Returns a set of nodes if a path between nodes exists, otherwise null.
        /// </summary>
        /// <param name="from"></param>
        /// <param name="to"></param>
        /// <param name="adjacencies"></param>
        /// <returns></returns>
        IEnumerable<Node> FindShortestPath(Node from, Node to, IEnumerable<Adjacency> adjacencies);
    }
}
