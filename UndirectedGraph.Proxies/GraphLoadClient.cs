﻿using System.ServiceModel;
using UndirectedGraph.Contracts.Data;
using UndirectedGraph.Contracts.Service;

namespace UndirectedGraph.Proxies
{
    public class GraphLoadClient : ClientBase<IGraphLoadService>, IGraphLoadService
    {
        public GraphData LoadGraph()
        {
            return Channel.LoadGraph();
        }
    }
}
