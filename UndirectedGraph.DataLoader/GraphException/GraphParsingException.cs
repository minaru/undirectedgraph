﻿using System;

namespace UndirectedGraph.DataLoader.GraphException
{
    class GraphParsingException : GraphException
    {
        public GraphParsingException(string nodeSourceDescription, Exception innerException) : base( string.Format("The following node could not be parsed: '{0}'.", nodeSourceDescription), innerException )
        {

        }
    }
}
