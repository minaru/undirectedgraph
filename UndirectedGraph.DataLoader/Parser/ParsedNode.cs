﻿using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;

namespace UndirectedGraph.DataLoader.Parser
{
    [XmlRoot(ElementName = "node")]
    public class ParsedNode
    {
        public ParsedNode()
        {

        }

        public ParsedNode(int id, string label, params int[] adjacentIds)
        {
            Id = id;
            Label = label;
            AdjacentIds = adjacentIds.ToList();
        }

        [XmlElement(ElementName = "id")]
        public int? NullableId { get; set; }

        [XmlIgnore]
        public int Id
        {
            get { return NullableId ?? 0; }
            set { NullableId = value; }
        }

        [XmlElement(ElementName = "label")]
        public string Label { get; set; }

        [XmlArray(ElementName = "adjacentNodes")]
        [XmlArrayItem(ElementName = "id")]
        public List<int> AdjacentIds { get; set; }
    }

    
}
