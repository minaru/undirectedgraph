﻿using System.Data.Entity;
using UndirectedGraph.Data.Entities;

namespace UndirectedGraph.Data
{
    public class GraphDbContext : DbContext
    {
        public DbSet<Node> NodeSet { get; set; }
        public DbSet<Adjacency> AdjacencySet { get; set; }

        public GraphDbContext() : base("name=UndirectedGraphConnectionString")
        {
            
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Node>()
                        .HasKey(n => n.Id);

            modelBuilder.Entity<Adjacency>()
                        .HasKey(a => a.Id)
                        .HasRequired(a => a.Node1).WithMany().HasForeignKey(a => a.Node1Id).WillCascadeOnDelete(true);
            modelBuilder.Entity<Adjacency>()
                        .HasRequired(a => a.Node2).WithMany().HasForeignKey(a => a.Node2Id).WillCascadeOnDelete(false);
        }
    }
}
