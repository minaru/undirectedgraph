﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using UndirectedGraph.Data.Entities;

namespace UndirectedGraph.Data.Repositories
{
    public class GraphRepository : IGraphRepository
    {
        private readonly GraphDbContext _context;

        public GraphRepository(GraphDbContext context)
        {
            if (context == null)
                throw new ArgumentNullException("context");

            _context = context;
        }

        public IEnumerable<Adjacency> LoadAllAdjacencies()
        {
            return _context.AdjacencySet.ToList();
        }

        public IEnumerable<Adjacency> LoadAllAdjacenciesWithNodes()
        {
            return _context.AdjacencySet
                            .Include(a => a.Node1)
                            .Include(a => a.Node2)
                            .ToList();
        }

        public IEnumerable<Node> LoadAllNodes()
        {
            return _context.NodeSet.ToList();
        }

        public void ReplaceAndSave(IEnumerable<Node> nodes, IEnumerable<Adjacency> adjacencies)
        {
            using (var transaction = _context.Database.BeginTransaction())
            {
                try
                {
                    _context.AdjacencySet.RemoveRange(_context.AdjacencySet);
                    _context.SaveChanges();

                    _context.NodeSet.RemoveRange(_context.NodeSet);
                    _context.SaveChanges();

                    _context.NodeSet.AddRange(nodes);
                    _context.SaveChanges();

                    foreach (var adjacency in adjacencies)
                    {
                        adjacency.Node1Id = adjacency.Node1.Id;
                        adjacency.Node2Id = adjacency.Node2.Id;
                    }

                    _context.AdjacencySet.AddRange(adjacencies);
                    _context.SaveChanges();

                    transaction.Commit();
                }
                catch
                {
                    transaction.Rollback();
                    throw;
                }
            }
        }
    }
}
