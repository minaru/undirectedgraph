﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace UndirectedGraph.Contracts.Data
{
    [DataContract]
    public class GraphData
    {
        [DataMember]
        public IEnumerable<NodeData> Nodes { get; set; }

        [DataMember]
        public IEnumerable<NodeAdjacencyData> NodeAdjacencies { get; set; }
    }

    public class NodeData
    {
        public int Id { get; set; }
        public string Label { get; set; }
    }

    public class NodeAdjacencyData
    {
        public int Node1Id { get; set; }
        public int Node2Id { get; set; }
    }
}
