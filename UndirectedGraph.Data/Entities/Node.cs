﻿using System.Collections.Generic;

namespace UndirectedGraph.Data.Entities
{
    public class Node
    {
        public Node()
        {

        }

        public Node(int externalId, string label)
        {
            ExternalId = externalId;
            Label = label;
        }

        public int Id { get; set; }
        public int ExternalId { get; set; }
        public string Label { get; set; }
    }
}
