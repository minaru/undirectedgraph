﻿using UndirectedGraph.DataLoader.Parser;

namespace UndirectedGraph.DataLoader.GraphException
{
    class SameNodeIdException : GraphException
    {
        public int Id { get; set; }
        public string Label1 { get; set; }
        public string Label2 { get; set; }

        public SameNodeIdException(int id, string label1, string label2) : base(string.Format("Same id: '{0}' was applied for nodes with labels: '{1}' and '{2}'.", id, label1, label2))
        {
            Id = id;
            Label1 = label1;
            Label2 = label2;
        }
    }
}
