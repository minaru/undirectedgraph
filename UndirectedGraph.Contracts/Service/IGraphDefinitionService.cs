﻿using System.ServiceModel;
using UndirectedGraph.Contracts.Data;

namespace UndirectedGraph.Contracts.Service
{
    [ServiceContract]
    public interface IGraphDefinitionService
    {
        /// <summary>
        /// Saves the given graph and replaces the existing one (if any).
        /// </summary>
        /// <param name="graphData"></param>
        [OperationContract]
        void SaveGraph(GraphData graphData);
    }
}
