﻿using System.Collections.Generic;
using System.ServiceModel;
using UndirectedGraph.Contracts.Data;

namespace UndirectedGraph.Contracts.Service
{
    [ServiceContract]
    public interface IGraphPathService
    {
        /// <summary>
        /// Returns a set of nodes if a path between nodes exists, otherwise null.
        /// </summary>
        /// <param name="from"></param>
        /// <param name="to"></param>
        /// <returns></returns>
        [OperationContract]
        IEnumerable<NodeData> FindShortestPath(NodeData from, NodeData to);
    }
}
