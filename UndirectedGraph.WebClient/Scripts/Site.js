﻿$(document).ready(function () {

    $('#calculatePathButton').prop('disabled', true);

    var _this = this;
    $('#calculatePathButton').click(calculateShortestPath);

    var options = {
        edges: {
            selectionWidth: 0
        },
        interaction: {
            multiselect: true
        },
        nodes: {
            shape: 'circle'
        }

    }
    this.nodes = new vis.DataSet(options);
    this.edges = new vis.DataSet(options);
    this.selectedIds = [];
    this.network = null;

    $.get("Graph/Load", function (graphResult) {
        if (graphResult.IsError) {
            showDescription('An error occured while loading graph.');
            return;
        }
        else {
            showDescription('');
        }
        showGraph(graphResult.Graph);
    });

    function showGraph(graph) {
        
        graph.Nodes.forEach(function (node) {
            _this.nodes.add({ id: node.Id, label: node.Label });
        });

        graph.NodeAdjacencies.forEach(function (adjacency) {
            _this.edges.add({ from: adjacency.Node1Id, to: adjacency.Node2Id });
        });
        
        var container = document.getElementById('graph');

        var data = {
            nodes: _this.nodes,
            edges: _this.edges
        };

        _this.network = new vis.Network(container, data, options);
        _this.network.on('selectNode', selectedNodesChanged);
        _this.network.on('deselectNode', selectedNodesChanged);
        
    }

    function selectedNodesChanged(eventArgs) {
        var selectedNodes = eventArgs.nodes;
        var isPathCalculationEnable = selectedNodes.length == 2;

        $('#calculatePathButton').prop('disabled', !isPathCalculationEnable);
        showDescription(' ');
    }

    function calculateShortestPath() {
        var selectedNodeIds = _this.network.getSelectedNodes();
        if (selectedNodeIds.length != 2) {
            return;
        }

        _shortPathCtx = this;
        _shortPathCtx.network = _this.network;
        _shortPathCtx.nodes = _this.nodes;

        var selectedNodes = _this.nodes.get(selectedNodeIds);
        var nodeData1 = nodeData(selectedNodes[0].id, selectedNodes[0].label);
        var nodeData2 = nodeData(selectedNodes[1].id, selectedNodes[1].label);
        _shortPathCtx.data = {
            fromNode: nodeData1,
            toNode: nodeData2
        };

        $.post("Graph/CalculateShortestPath", _shortPathCtx.data, function (pathResult) {
            if (pathResult.IsError) {
                showDescription('An error occured while loading shortest path.');
                return;
            }

            var path = pathResult.Path;
            var fromLabel = _shortPathCtx.data.fromNode.Label;
            var toLabel = _shortPathCtx.data.toNode.Label;

            if (path == null) {
                showDescription('Path between "' + fromLabel + '" and "' + toLabel + '" does not exist.');
                return;
            }

            var pathNodeIds = [];
            pathNodeIds.push(_shortPathCtx.data.fromNode.Id);
            for (var i = 0; i < path.length; i++) {
                pathNodeIds.push(path[i].Id);
            }

            _shortPathCtx.network.selectNodes(pathNodeIds);
            $('#calculatePathButton').prop('disabled', true);
            showDescription('Path between "' + fromLabel + '" and "' + toLabel + '" is ' + path.length + ' hop' + (path.length > 1 ? 's' : '') + ' long.');

        });
        
    }

    function showDescription(description) {
        $('#description').text(description);
    }

    function nodeData(id, label) {
        return {
            Id: id,
            Label: label
        };
    }

});

