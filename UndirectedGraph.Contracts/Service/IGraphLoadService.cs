﻿using System.ServiceModel;
using UndirectedGraph.Contracts.Data;

namespace UndirectedGraph.Contracts.Service
{
    [ServiceContract]
    public interface IGraphLoadService
    {
        /// <summary>
        /// Loads the whole graph including nodes and adjacencies.
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        GraphData LoadGraph();
    }
}
