﻿using System;
using System.IO;
using System.Xml.Serialization;

namespace UndirectedGraph.DataLoader.Parser
{
    internal class NodeParser : INodeParser
    {
        public ParsedNode ParseNode(Stream stream)
        {
            if (stream == null)
                throw new ArgumentNullException("stream");

            XmlSerializer x = new XmlSerializer(typeof(ParsedNode));
            try
            {
                var parsedNode = (ParsedNode) x.Deserialize(stream);
                if (parsedNode.NullableId == null || parsedNode.Label == null)
                    throw new FormatException("An element is missing.");

                return parsedNode;
            }
            catch(Exception exception)
            {
                throw new FormatException("An error occured while parsing xml.", exception);
            }
        }
    }
}
