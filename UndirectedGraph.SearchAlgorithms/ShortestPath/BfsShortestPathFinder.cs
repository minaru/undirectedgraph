﻿using System;
using System.Collections.Generic;
using System.Linq;
using UndirectedGraph.Data.Entities;

namespace UndirectedGraph.SearchAlgorithms.ShortestPath
{
    public class BfsShortestPathFinder : IShortestPathFinder
    {
        public IEnumerable<Node> FindShortestPath(Node from, Node to, IEnumerable<Adjacency> adjacencies)
        {
            from.GuardNotNull();
            to.GuardNotNull();
            adjacencies.GuardNotNull();

            if (from.ExternalId == to.ExternalId)
                return new Node[] { };

            var finder = new Finder(adjacencies);
            var path = finder.FindShortestPath(from, to);

            return path;
        }

        private class Finder
        {
            private readonly Dictionary<int, Node> _previousNodeDict;
            private readonly Dictionary<int, IList<Node>> _adjacentNodesDict;

            public Finder(IEnumerable<Adjacency> adjacencies)
            {
                _previousNodeDict = new Dictionary<int, Node>();
                _adjacentNodesDict = CalculateAdjacentNodes(adjacencies);
            }

            public IEnumerable<Node> FindShortestPath(Node from, Node to)
            {
                var destinationNode = FindDestinationNode(from, to);
                if (destinationNode == null)
                    return null;

                var path = CalculateReversedPath(destinationNode, from);

                return path.Reverse();
            }

            private IEnumerable<Node> CalculateReversedPath(Node destinationNode, Node startNode)
            {
                var path = new List<Node>();

                var currentNode = destinationNode;
                while (currentNode.ExternalId != startNode.ExternalId)
                {
                    path.Add(currentNode);
                    var previousNode = _previousNodeDict[currentNode.ExternalId];
                    currentNode = previousNode;
                }
                return path;
            }

            private Node FindDestinationNode(Node from, Node to)
            {
                var queue = new Queue<Node>();
                queue.Enqueue(from);

                while(queue.Count > 0)
                {
                    var node = queue.Dequeue();
                    if (node.ExternalId == to.ExternalId)
                        return node;

                    var adjacentNodes = GetAdjacentNodes(node);
                    foreach (var adjNode in adjacentNodes)
                    {
                        if (_previousNodeDict.ContainsKey(adjNode.ExternalId))
                            continue;

                        _previousNodeDict.Add(adjNode.ExternalId, node);
                        queue.Enqueue(adjNode);
                    }
                }

                return null;
            }

            private IEnumerable<Node> GetAdjacentNodes(Node node)
            {
                IList<Node> adjacentNodes = null;
                _adjacentNodesDict.TryGetValue(node.ExternalId, out adjacentNodes);

                return adjacentNodes ?? new List<Node>(0);
            }

            private Dictionary<int, IList<Node>> CalculateAdjacentNodes(IEnumerable<Adjacency> adjacencies)
            {
                var adjDict = new Dictionary<int, IList<Node>>();
                foreach (var adj in adjacencies)
                {
                    var key1 = adj.Node1.ExternalId;
                    if (!adjDict.ContainsKey(key1))
                        adjDict.Add(key1, new List<Node>());
                    adjDict[key1].Add(adj.Node2);

                    var key2 = adj.Node2.ExternalId;
                    if (!adjDict.ContainsKey(key2))
                        adjDict.Add(key2, new List<Node>());
                    adjDict[key2].Add(adj.Node1);
                }

                return adjDict;
            }

        }
    }

    static class ObjectExtensions
    {
        public static void GuardNotNull(this object o)
        {
            if (o == null)
                throw new ArgumentNullException();
        }
    }
}
