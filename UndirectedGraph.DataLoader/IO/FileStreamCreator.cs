﻿using System;
using System.IO;

namespace UndirectedGraph.DataLoader.IO
{
    internal class FileStreamCreator : IStreamCreator
    {
        private readonly string _filePath;
        private FileStream _fileStream;

        public FileStreamCreator(string filePath)
        {
            if (string.IsNullOrEmpty(filePath))
                throw new ArgumentNullException("filePath");

            _filePath = filePath;
        }

        public string StreamSourceDescription
        {
            get
            {
                return _filePath;
            }
        }

        public Stream CreateStream()
        {
            _fileStream = File.OpenRead(_filePath);
            return _fileStream;
        }

        public void Dispose()
        {
            _fileStream.Close();
        }
    }
}
