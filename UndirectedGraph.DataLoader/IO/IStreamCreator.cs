﻿using System;
using System.IO;

namespace UndirectedGraph.DataLoader.IO
{
    public interface IStreamCreator : IDisposable
    {
        string StreamSourceDescription { get; }

        Stream CreateStream();
    }
}
