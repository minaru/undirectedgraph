﻿using System;
using UndirectedGraph.DataLoader.IO;
using Xunit;

namespace UndirectedGraph.DataLoader.UnitTests
{
    public class FileStreamCreatorFacts
    {

        public class TheConstructor
        {
            [Fact]
            public void ThrowsArgumentNullExceptionForNullFilePathParameter()
            {
                Assert.Throws<ArgumentNullException>(() => new FileStreamCreator(null));
            }

            [Fact]
            public void ThrowsArgumentNullExceptionForEmptyFilePathParameter()
            {
                Assert.Throws<ArgumentNullException>(() => new FileStreamCreator(""));
            }
        }
    }
}
