﻿using System.IO;

namespace UndirectedGraph.DataLoader.Parser
{
    public interface INodeParser
    {
        /// <summary>
        /// Parses XML document into a ParsedNode object.
        /// </summary>
        /// <param name="stream">Stream with the XML document.</param>
        /// <returns></returns>
        ParsedNode ParseNode(Stream stream);
    }
}
