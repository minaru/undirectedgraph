# Undirected graph #
Undirected graph is a set of projects for graph loading, saving, vizualizing and traversing.

## Requirements ##
* .NET framework 4.6.1
* MS SQL Server 2008 Express (or higher)
* Visual Studio 2015

## Setup ##
1. Create a database
2. Apply the following script to create schema: UndirectedGraph.Database\CreateSchema.sql
3. Open UndirectedGraph.sln in Visual Studio
4. Update connection string (name=UndirectedGraphConnectionString) in: UndirectedGraph.WebService\Web.config:7
5. Build solution
6. Start UndirectedGraph.WebService
7. Start UndirectedGraph.DataLoader with path to graph directory as a parameter
8. Start UndirectedGraph.WebClient

## Main components ##
* **UndirectedGraph.DataLoader**
    * Parses graph from *.xml files.
    * Stores loaded graph via WCF service.
* **UndirectedGraph.WebService**
    * Saves graph received via WCF into MSSQL database.
    * Provides saved graph via WCF service.
    * Provides shortest path calculation algorithm via WCF service.
* **UndirectedGraph.WebClient**
    * Visualizes graph loaded via WCF.
    * Allows users to calculate shortest path between nodes.

### UndirectedGraph.DataLoader ###
* DataLoader is a console project that requires one parameter: a path to directory cotaining graph *.xml files.
    * Example: UndirectedGraph.DataLoader.exe "c:\Graph"
* In case UndirectedGraph.WebService has a different address, please amend "UndirectedGraph.DataLoader\App.config" accordingly:

```
#!xml

<endpoint address="http://localhost:58360/GraphService.svc"
                binding="basicHttpBinding"
                contract="UndirectedGraph.Contracts.Service.IGraphDefinitionService" />
```

### UndirectedGraph.WebService ###
* It is a web application, which can be hosted in IIS server.
* WebService uses a database, therefore its connection string must be amended: (name=UndirectedGraphConnectionString) in: UndirectedGraph.WebService\Web.config:7
```
#!xml

<connectionStrings>
    <add name="UndirectedGraphConnectionString" connectionString="Data Source=.\sqlexpress;Initial Catalog=UndirectedGraph;Integrated Security=True" providerName="System.Data.SqlClient" />
</connectionStrings>
```

### UndirectedGraph.WebClient ###
* It is a web application, which can be hosted in IIS server.
* WebClient communicates with WebService project via WCF. In case UndirectedGraph.WebService has a different address, please amend "UndirectedGraph.DataLoader\App.config" accordingly:
```
#!xml

<endpoint address="http://localhost:58360/GraphService.svc"
                binding="basicHttpBinding"
                contract="UndirectedGraph.Contracts.Service.IGraphLoadService" />
<endpoint address="http://localhost:58360/GraphService.svc"
				binding="basicHttpBinding"
				contract="UndirectedGraph.Contracts.Service.IGraphPathService" />
```

## Shortest path calculation ##
To select two nodes to perform the shortest path search, please hold "Ctrl" key while clicking on nodes.


## Missing parts ##
* UndirectedGraph.DataLoader
    * Does not monitor directory for file changes. To update graph according to the new file setup, please re-run the DataLoader again.
	* Xsd schema for graph files. Even though the schema is missing, the files are validated properly.
* Logging
    * Current implementation does not contain any logging.