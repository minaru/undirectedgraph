﻿using System;
using System.Collections.Generic;
using System.Linq;
using UndirectedGraph.Contracts.Data;
using UndirectedGraph.DataLoader.GraphException;
using UndirectedGraph.DataLoader.IO;
using UndirectedGraph.DataLoader.Parser;

namespace UndirectedGraph.DataLoader
{
    class GraphLoader
    {
        private readonly INodeParser _nodeParser;

        public GraphLoader(INodeParser nodeParser)
        {
            if (nodeParser == null)
                throw new ArgumentNullException("nodeParser");

            _nodeParser = nodeParser;
        }

        /// <summary>
        /// Loads Graph from streamCreators.
        /// Throws GraphAdjacencyException if a not existing node is referenced.
        /// Throws GraphParsingException in case of parsing issue.
        /// Throws SameNodeIdException if two nodes have same Id
        /// </summary>
        /// <param name="streamCreators"></param>
        /// <returns></returns>
        public GraphData LoadGraph(IEnumerable<IStreamCreator> streamCreators)
        {
            var parsedNodes = ParseNodes(streamCreators);
            var adjacencyIds = CalculateAdjacencies(parsedNodes);

            var nodes = ConvertToNodes(parsedNodes);
            var adjacencies = ConvertToNodeAdjacencies(nodes, adjacencyIds);

            return new GraphData
            {
                Nodes = nodes,
                NodeAdjacencies = adjacencies
            };
        }

        private IList<ParsedNode> ParseNodes(IEnumerable<IStreamCreator> streamCreators)
        {
            var nodes = streamCreators.Select( sc => 
                    {
                        using (var stream = sc.CreateStream())
                        {
                            try
                            {
                                var node = _nodeParser.ParseNode(stream);
                                return node;
                            }
                            catch (Exception exception)
                            {
                                throw new GraphParsingException(sc.StreamSourceDescription, exception);
                            }
                        }
                    } ).ToList();

            return nodes;
        }

        private IList<NodeData> ConvertToNodes(IList<ParsedNode> parsedNodes)
        {
            return parsedNodes.Select(pn => new NodeData { Id = pn.Id, Label = pn.Label }).ToList();
        }

        private IEnumerable<Tuple<int,int>> CalculateAdjacencies(IList<ParsedNode> parsedNodes)
        {
            var nodesDict = CreateNodesDictionary(parsedNodes);
            var adjacencySet = new HashSet<Tuple<int, int>>();
            foreach (var node in parsedNodes)
            {
                foreach(var adjacentId in node.AdjacentIds)
                {
                    if (!nodesDict.ContainsKey(adjacentId))
                        throw new GraphAdjacencyException(node, adjacentId);

                    var adjacency = (node.Id < adjacentId) ? Tuple.Create(node.Id, adjacentId) : Tuple.Create(adjacentId, node.Id);
                    adjacencySet.Add(adjacency);
                }
            }

            return adjacencySet;
        }

        private Dictionary<int, ParsedNode> CreateNodesDictionary(IEnumerable<ParsedNode> nodes)
        {
            var dictionary = new Dictionary<int, ParsedNode>();
            foreach(var node in nodes)
            {
                if (dictionary.ContainsKey(node.Id))
                    throw new SameNodeIdException(node.Id, node.Label, dictionary[node.Id].Label);
                dictionary.Add(node.Id, node);
            }

            return dictionary;
        }

        private IEnumerable<NodeAdjacencyData> ConvertToNodeAdjacencies(IEnumerable<NodeData> nodes, IEnumerable<Tuple<int, int>> adjacencyIds)
        {
            var adjacencies = adjacencyIds
                               .Select(a => new NodeAdjacencyData
                               {
                                   Node1Id = a.Item1,
                                   Node2Id = a.Item2
                               }).ToList();
            return adjacencies;
        }
    }
}
