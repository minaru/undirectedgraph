﻿using Autofac;
using Autofac.Integration.Wcf;
using System;
using System.Web;
using UndirectedGraph.Data;
using UndirectedGraph.Data.Repositories;
using UndirectedGraph.SearchAlgorithms.ShortestPath;

namespace UndirectedGraph.WebService
{
    public class Global : HttpApplication
    {

        protected void Application_Start(object sender, EventArgs e)
        {
            var builder = new ContainerBuilder();
            builder.RegisterType<GraphManager>();
            builder.RegisterType<BfsShortestPathFinder>().As<IShortestPathFinder>();
            builder.RegisterType<GraphRepository>().As<IGraphRepository>();
            builder.RegisterType<GraphDbContext>().InstancePerLifetimeScope(); 

            AutofacHostFactory.Container = builder.Build();

        }

        protected void Session_Start(object sender, EventArgs e)
        {

        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {

        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {

        }

        protected void Application_Error(object sender, EventArgs e)
        {

        }

        protected void Session_End(object sender, EventArgs e)
        {

        }

        protected void Application_End(object sender, EventArgs e)
        {

        }
    }
}