﻿using System;
using System.IO;
using System.Linq;
using System.Text;
using UndirectedGraph.DataLoader.Parser;
using Xunit;

namespace UndirectedGraph.DataLoader.UnitTests
{
    public class NodeParserFacts
    {

        public class TheParseNodeMethod
        {
            private readonly NodeParser _nodeParser;

            public TheParseNodeMethod()
            {
                _nodeParser = new NodeParser();
            }

            [Fact]
            public void ThrowsArgumentNullExceptionForNullStreamArgument()
            {
                Assert.Throws<ArgumentNullException>(() => _nodeParser.ParseNode(null) );
            }

            [Fact]
            public void ThrowsFormatExceptionForInvalidXml()
            {
                string xml = @"<?xml version=""1.0"" encoding=""utf-8""?><node><id>4</id><label>IBM</label><adjacentNodes><id>10</id><id>11</id><id>15</id></adjacentNodes>";

                Assert.Throws<FormatException>(() => _nodeParser.ParseNode(xml.ToStream()));
            }

            [Fact]
            public void ThrowsFormatExceptionForNaNId()
            {
                string xml = @"<?xml version=""1.0"" encoding=""utf-8""?><node><id>IBM</id><label>IBM</label><adjacentNodes /></node>";

                Assert.Throws<FormatException>(() => _nodeParser.ParseNode(xml.ToStream()));
            }

            [Fact]
            public void ThrowsFormatExceptionForMissingIdElement()
            {
                string xml = @"<?xml version=""1.0"" encoding=""utf-8""?><node><label>IBM</label><adjacentNodes /></node>";

                Assert.Throws<FormatException>(() => _nodeParser.ParseNode(xml.ToStream()));
            }

            [Fact]
            public void ThrowsFormatExceptionForMissingLabelElement()
            {
                string xml = @"<?xml version=""1.0"" encoding=""utf-8""?><node><id>1</id><adjacentNodes /></node>";

                Assert.Throws<FormatException>(() => _nodeParser.ParseNode(xml.ToStream()));
            }

            [Fact]
            public void ReturnsNodeWithoutAdjacencies()
            {
                string xml = @"<?xml version=""1.0"" encoding=""utf-8""?><node><id>4</id><label>IBM</label><adjacentNodes /></node>";

                var result = _nodeParser.ParseNode(xml.ToStream());

                Assert.Equal(4, result.Id);
                Assert.Equal("IBM", result.Label);
                Assert.Empty(result.AdjacentIds);
            }

            [Fact]
            public void ReturnsNodeWithOneAdjacency()
            {
                string xml = @"<?xml version=""1.0"" encoding=""utf-8""?><node><id>4</id><label>IBM</label><adjacentNodes><id>10</id></adjacentNodes></node>";

                var result = _nodeParser.ParseNode(xml.ToStream());

                Assert.Equal(4, result.Id);
                Assert.Equal("IBM", result.Label);
                Assert.Equal(1, result.AdjacentIds.Count());
                Assert.Equal(10, result.AdjacentIds.Single());
            }

            [Fact]
            public void ReturnsNodeWithMultipleAdjacencies()
            {
                string xml = @"<?xml version=""1.0"" encoding=""utf-8""?><node><id>4</id><label>IBM</label><adjacentNodes><id>10</id><id>11</id><id>15</id></adjacentNodes></node>";

                var result = _nodeParser.ParseNode(xml.ToStream());

                Assert.Equal(3, result.AdjacentIds.Count());
                Assert.Contains(10, result.AdjacentIds);
                Assert.Contains(11, result.AdjacentIds);
                Assert.Contains(15, result.AdjacentIds);
            }

            [Fact]
            public void ReturnsNodeWithNoAdjacenciesForXmlWithoutAdjacentNodesElement()
            {
                string xml = @"<node><id>4</id><label>IBM</label><adjacentNodes><id>10</id><id>11</id><id>15</id></adjacentNodes></node>";

                var result = _nodeParser.ParseNode(xml.ToStream());

                Assert.Equal(3, result.AdjacentIds.Count());
                Assert.Contains(10, result.AdjacentIds);
                Assert.Contains(11, result.AdjacentIds);
                Assert.Contains(15, result.AdjacentIds);
            }

            [Fact]
            public void ThrowsFormatExceptionForMissingAdjacentNodesElement()
            {
                string xml = @"<?xml version=""1.0"" encoding=""utf-8""?><node><id>4</id><label>IBM</label></node>";

                var result = _nodeParser.ParseNode(xml.ToStream());

                Assert.Equal(4, result.Id);
                Assert.Equal("IBM", result.Label);
            }
        }

    }

    public static class StringStreamExtensions
    {
        public static Stream ToStream(this string s)
        {
            byte[] byteArray = Encoding.UTF8.GetBytes(s);
            return new MemoryStream(byteArray);
        }
    }
}
