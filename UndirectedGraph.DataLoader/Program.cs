﻿using System;
using System.IO;
using System.Linq;
using UndirectedGraph.DataLoader.GraphException;
using UndirectedGraph.DataLoader.IO;
using UndirectedGraph.DataLoader.Parser;
using UndirectedGraph.Proxies;

namespace UndirectedGraph.DataLoader
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length != 1)
            {
                ShowHelp();
                return;
            }

            var directoryPath = args[0];
            if (!Directory.Exists(directoryPath))
            {
                ShowDirectoryNotFound(directoryPath);
                return;
            }

            var filePaths = Directory.GetFiles(directoryPath, "*.xml");
            if (filePaths.Length <= 0)
            {
                ShowDirectoryIsEmpty(directoryPath);
                return;
            }

            try
            {
                var nodeParser = new NodeParser();
                var graphLoader = new GraphLoader(nodeParser);
                var streamCreators = filePaths.Select(fp => new FileStreamCreator(fp));
                var graph = graphLoader.LoadGraph(streamCreators);

                var client = new GraphDefinitionClient();
                client.SaveGraph(graph);

                Console.WriteLine("Graph has been successfully loaded and saved.");
            }
            catch(GraphParsingException exception)
            {
                Console.WriteLine(exception.Message);
            }
            catch (GraphAdjacencyException exception)
            {
                Console.WriteLine(exception.Message);
            }
            catch (SameNodeIdException exception)
            {
                Console.WriteLine(exception.Message);
            }
            catch(Exception exception)
            {
                Console.WriteLine("The following unknown error occured: '{0}'.", exception);
            }
        }

        private static void ShowHelp()
        {
            Console.WriteLine("Please define path to a directory containing graph .xml files.");
            Console.WriteLine(string.Format("Example: {0} c:\\Graph"), AppDomain.CurrentDomain.FriendlyName);
        }

        private static void ShowDirectoryNotFound(string directoryPath)
        {
            Console.WriteLine(string.Format("Directory '{0}' was not found.", directoryPath));
        }

        private static void ShowDirectoryIsEmpty(string directoryPath)
        {
            Console.WriteLine(string.Format("Directory '{0}' is empty or doesn't contain .xml files.", directoryPath));
        }
    }
}
