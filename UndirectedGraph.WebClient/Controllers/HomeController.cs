﻿using System.Web.Mvc;

namespace UndirectedGraph.WebClient.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
    }
}