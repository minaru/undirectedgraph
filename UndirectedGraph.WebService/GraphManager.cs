﻿using System;
using System.Collections.Generic;
using System.Linq;
using UndirectedGraph.Contracts.Data;
using UndirectedGraph.Contracts.Service;
using UndirectedGraph.Data.Entities;
using UndirectedGraph.Data.Repositories;
using UndirectedGraph.SearchAlgorithms.ShortestPath;

namespace UndirectedGraph.WebService
{
    public class GraphManager : IGraphDefinitionService, IGraphPathService, IGraphLoadService
    {
        private readonly IGraphRepository _graphRepository;
        private readonly IShortestPathFinder _pathFinder;

        public GraphManager(IGraphRepository graphRepository, IShortestPathFinder pathFinder)
        {
            if (graphRepository == null)
                throw new ArgumentNullException("graphRepository");

            if (pathFinder == null)
                throw new ArgumentNullException("pathFinder");

            _graphRepository = graphRepository;
            _pathFinder = pathFinder;
        }

        #region IGraphDefinitionService implementation

        public void SaveGraph(GraphData graphData)
        {
            if (graphData == null)
                throw new ArgumentNullException("graphData");

            var nodes = graphData.Nodes.Select(n => new Node
            {
                ExternalId = n.Id,
                Label = n.Label
            }).ToList();

            var nodesDictionary = nodes.ToDictionary(n => n.ExternalId);
            var adjacencies = graphData.NodeAdjacencies.Select(a => new Adjacency
            {
                Node1 = nodesDictionary[a.Node1Id],
                Node2 = nodesDictionary[a.Node2Id]
            }).ToList();

            _graphRepository.ReplaceAndSave(nodes, adjacencies);
        }

        #endregion

        #region IGraphPathService implementation

        public IEnumerable<NodeData> FindShortestPath(NodeData from, NodeData to)
        {
            if (from == null)
                throw new ArgumentNullException("from");

            if (to == null)
                throw new ArgumentNullException("to");

            var adjacencies = _graphRepository.LoadAllAdjacenciesWithNodes();
            var fromNode = new Node(from.Id, from.Label);
            var toNode = new Node(to.Id, to.Label);

            var pathNodes = _pathFinder.FindShortestPath(fromNode, toNode, adjacencies);

            if (pathNodes == null)
                return null;

            var pathNodesData = pathNodes.Select(pn => new NodeData { Id = pn.ExternalId, Label = pn.Label });
            return pathNodesData;
        }

        #endregion

        #region IGraphLoadService implementation

        public GraphData LoadGraph()
        {
            var nodes = _graphRepository.LoadAllNodes();
            var adjacencies = _graphRepository.LoadAllAdjacenciesWithNodes();

            var nodesData = nodes.Select(n => new NodeData { Id = n.ExternalId, Label = n.Label });
            var adjacenciesData = adjacencies.Select(n => new NodeAdjacencyData { Node1Id = n.Node1.ExternalId, Node2Id = n.Node2.ExternalId });

            return new GraphData { Nodes = nodesData, NodeAdjacencies = adjacenciesData };
        }

        #endregion
    }
}