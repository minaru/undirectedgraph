﻿using System.ServiceModel;
using UndirectedGraph.Contracts.Data;
using UndirectedGraph.Contracts.Service;

namespace UndirectedGraph.Proxies
{
    public class GraphDefinitionClient : ClientBase<IGraphDefinitionService>, IGraphDefinitionService
    {
        public void SaveGraph(GraphData graphData)
        {
            Channel.SaveGraph(graphData);
        }
    }
}
