﻿using System.Collections.Generic;
using UndirectedGraph.Data.Entities;

namespace UndirectedGraph.Data.Repositories
{
    public interface IGraphRepository
    {
        /// <summary>
        /// Persists nodes and adjacencies.
        /// Previous nodes and adjacencies are replaced by the new ones.
        /// </summary>
        /// <param name="nodes"></param>
        /// <param name="adjacencies"></param>
        void ReplaceAndSave(IEnumerable<Node> nodes, IEnumerable<Adjacency> adjacencies);

        /// <summary>
        /// Loads all Nodes without Adjacencies.
        /// </summary>
        /// <returns></returns>
        IEnumerable<Node> LoadAllNodes();

        /// <summary>
        /// Loads all Adjacencies without Nodes.
        /// </summary>
        /// <returns></returns>
        IEnumerable<Adjacency> LoadAllAdjacencies();

        /// <summary>
        /// Loads all Adjacencies with Nodes.
        /// </summary>
        /// <returns></returns>
        IEnumerable<Adjacency> LoadAllAdjacenciesWithNodes();
    }
}
