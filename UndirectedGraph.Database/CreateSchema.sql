
create table Nodes
(
	Id int not null identity(1,1),
	ExternalId int not null,
	Label varchar(200) not null,
	constraint PK_Nodes_Id primary key (Id),
	constraint AK_Nodes_ExternalId unique(ExternalId)
)

go

create table Adjacencies
(
	Id int not null identity(1,1),
	Node1Id int not null,
	Node2Id int not null,
	constraint PK_Adjacencies_Id primary key (Id),
	constraint AK_Adjacencies_Node1Id_Node2Id unique(Node1Id, Node2Id),
	constraint FK_Adjacencies_Nodes_Node1Id foreign key (Node1Id) references Nodes(Id) on delete cascade,
	constraint FK_Adjacencies_Nodes_Node2Id foreign key (Node2Id) references Nodes(Id)
)

go
