﻿using System;
using UndirectedGraph.DataLoader.Parser;

namespace UndirectedGraph.DataLoader.GraphException
{
    class GraphAdjacencyException : GraphException
    {
        public ParsedNode Node { get; private set; }

        public int AdjacencyId { get; private set; }

        public GraphAdjacencyException(ParsedNode node, int adjacencyId) : base(string.Format("Node id: '{0}', label: '{1}' has adjacency with an unknown node id: '{2}'.", node.Id, node.Label, adjacencyId))
        {
            Node = node;
            AdjacencyId = adjacencyId;
        }
    }
}
